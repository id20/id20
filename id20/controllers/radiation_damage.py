
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy as np
from bliss.scanning.chain import ChainPreset, ChainIterationPreset
from bliss.common.counter import SoftCounter
from bliss.config.settings import HashObjSetting
from bliss.common.axis import Axis
from bliss.shell.standard import flint
from id26.controllers.plot_radiation_damage import PositionManagerPlotInBliss


# def enable_radiation_damage_(positions_manager, pattern_name, xmot, ymot):
#     rd = RadiationDamagePreset(xmot, ymot, positions_manager, pattern_name)
#     DEFAULT_CHAIN.add_preset(rd)


# from id26.controllers.radiation_damage import PositionManager, RadiationDamagePreset  
# ...: pm=PositionManager('pm1')                                                         
# ...: pm.add_rect_pattern('rect1', -5, -4,1,1,10, 8, centered=False, snake=False)  
# ...: rd = RadiationDamagePreset(TSY,TSZ,pm,'rect1')                                
# ...: s = dscan(xes_en_texs, .002, -.002, 10, .5,  run=False)                   
# ...: s.acq_chain.add_preset(rd)

ecolors = ['k', 'b', 'magenta', 'g','orange', 'y', 'brown','pink','grey']




def axes2counters(*axes):
    scs = []
    for axis in axes:
        sc = SoftCounter(axis, 'position', name=f"{axis.name}_cnt")
        sc.mode = 'SINGLE'
        scs.append(sc)

    return scs


class RadiationDamagePreset(ChainPreset):

    # ===== usage ==============

    # p = RadiationDamagePreset()
    # s = loopscan(2,0.1,diode,run=False)
    # s.acq_chain.add_preset(p)
    # s.run()

    class Iterator(ChainIterationPreset):
        def __init__(self, boss):
            self.boss = boss

        def prepare(self):
            pass

        def start(self):
            x, y = self.boss.pm.get_next_position(self.boss.pname)
            print(f"moving ({self.boss.xmot.name}, {self.boss.ymot.name}) to ({x}, {y}) (counts={self.boss.pm.get_cycle_count(self.boss.pname)})\n")
            self.boss.xmot.move(x, wait=False)
            self.boss.ymot.move(y, wait=True)
            self.boss.xmot.wait_move()

        def stop(self):
            pass
    
    def __init__(self, motor_x, motor_y, positions_manager, pattern_name):
        
        if not isinstance(positions_manager, PositionManager):
            raise ValueError(f"{positions_manager} is not a PositionManager object !")

        if pattern_name not in positions_manager._positions.keys():
            raise ValueError(f"PositionManager '{positions_manager._name}' doesn't have a pattern with name '{pattern_name}' !")
        
        if not isinstance(motor_x, Axis):
            raise ValueError(f"expecting motor_x as an Axis but receive {motor_x}")

        if not isinstance(motor_y, Axis):
            raise ValueError(f"expecting motor_y as an Axis but receive {motor_y}")

        self._xmot = motor_x
        self._ymot = motor_y
        self._pm = positions_manager
        self._pname = pattern_name

    @property    
    def xmot(self):
        return self._xmot

    @property    
    def ymot(self):
        return self._ymot

    @property    
    def pm(self):
        return self._pm

    @property    
    def pname(self):
        return self._pname

    def get_iterator(self, acq_chain):
        while True:
            yield RadiationDamagePreset.Iterator(self)


class PositionManager:

    #_PATTERNS = enum.Enum("PATTERNS", "RECT CIRCULAR")

    def __init__(self, name):
        """ Manage list of positions generated from rectangular or circular pattern.
            The pattern is divided into rectangular pixels (dx, dy).
            The stored positions correspond to the center of the pixels.
        """
        self._name = name
        self._positions = HashObjSetting(f"PositionManager_positions:{self._name}") # dict of positions list
        self._counts = HashObjSetting(f"PositionManager_counts:{self._name}")    # dict of position counts list
        self._last_index = HashObjSetting(f"PositionManager_index:{self._name}")   # dict of current position index
        self._exposure_times = HashObjSetting(f"PositionManager_exposures:{self._name}") # dict of exp time list
        
    def get_next_position(self, pname):
        if self.get_index(pname) == -1:
            self._set_index(pname, 0)
        else:
            val = self.get_index(pname) + 1
            self._set_index(pname, val)
            # restart at 0 if the last position index has been reached
            if self.get_index(pname) >= len(self._positions[pname]):
                self._set_index(pname, 0)

        i = self.get_index(pname)

        _counts = self._counts.get_all()
        _counts [pname][i]+=1
        self._counts.update(_counts)

        return self._positions[pname][i]

    
    def get_positions(self, pname):
        return self._positions[pname]

    def get_position(self, pname):
        i = self.get_index(pname)
        if i is None:
            raise RuntimeError(f"Call get_next_position() at least once to initialize position")
        return self._positions[pname][i]

    def get_position_from_index(self, pname, index):
        return self._positions[pname][index]

    def set_position(self, pname, px, py):
        index = self.get_index_from_position(pname, px, py)
        self._set_index(pname, index)


    def get_index(self, pname):
        return self._last_index[pname][0]
        
    def _set_index(self, pname, index):
        cidx, pargs = self._last_index[pname]
        self._last_index[pname] = [index, pargs]
    
    def get_index_from_position(self, pname, px, py):
        for i, pos in enumerate(self._positions[pname]):
            if pos == (px,py):
                return i
        try:
            # to check (rounding / positioning precision)
            tups = np.array(self._positions[pname])
            i = np.argmin(np.linalg.norm(np.abs(np.array(tups)-np.array([px, py])),axis=1))-1
            return i
        except:
            raise ValueError(f"cannot find the position {(px, py)} in '{pname}' positions")
            
    def set_index(self, pname, index):
        nbr_pos = len(self._positions[pname])
        if index>=0 and index<nbr_pos:
            self._set_index(pname, index)
        else:
            raise ValueError(f"index should be in [0,{len(self._positions[pname])}]")


    def get_cycle_counts(self, pname):
        return self._counts[pname]

    def get_cycle_count(self, pname):
        i = self.get_index(pname)
        if i is None:
            raise RuntimeError(f"Call get_next_position() at least once to initialize position")
        
        return self._counts[pname][i]

    def get_cycle_count_from_position(self, pname, px, py):
        index = self.get_index_from_position(pname, px, py)
        return self._counts[pname][index]

    def get_cycle_count_from_index(self, pname, index):
        return self._counts[pname][index]

    
    def store_exposure_time(self, pname, px, py, expo):
        index = self.get_index_from_position(pname, px, py)
        _values = self._exposure_times.get_all()
        _values [pname][index] = expo
        self._exposure_times.update(_values)


        
    def get_exposure_time(self, pname, px, py):
        index = self.get_index_from_position(pname, px, py)
        return self._exposure_times[pname][index]

    def add_rect_pattern(self, pname, xstart, ystart, xend, yend, dx, dy, extend_beyond_endpoint = False, snake=True, centered=False, overwrite=False):
        
        patt_args = (xstart, ystart, xend, yend, dx, dy, extend_beyond_endpoint, snake, centered)
        
        nx = abs(xend-xstart)/dx
        ny = abs(yend-ystart)/dy
        
        mytol = .001

        if abs(int(nx)-nx) > mytol and extend_beyond_endpoint:
            nx = int(nx) + 1
        else:
            nx = int(nx)

        if abs(int(ny)-ny) > mytol and extend_beyond_endpoint:
            ny = int(ny) + 1
        else:
            ny = int(ny)
 
        xx, yy = self._get_rect_positions(xstart, ystart, dx, dy, nx, ny, snake, centered)
        positions = list(zip(xx, yy))
        self._add_pattern(pname, positions, patt_args, overwrite=overwrite)

    def add_rect_pattern2(self, pname, xstart, ystart, dx, dy, nx, ny, snake=True, centered=False, overwrite=False):
        patt_args = (xstart, ystart, dx, dy, nx, ny, snake, centered)
        
        xx, yy = self._get_rect_positions(xstart, ystart, dx, dy, nx-1, ny-1, snake, centered)
        positions = list(zip(xx, yy))
        self._add_pattern(pname, positions, patt_args, overwrite=overwrite)

    def add_circular_pattern(self, pname, cx, cy, dx, dy, diameter, snake=True, centered=False, overwrite=False):
        patt_args = (cx, cy, dx, dy, diameter, snake, centered)
        
        r = diameter/2
        xstart = cx - r
        ystart = cy - r
        nx = diameter/dx
        ny = diameter/dy
        
        mytol = .001

        if abs(int(nx)-nx) > mytol:
            nx = int(nx) + 1
        else:
            nx = int(nx)

        if abs(int(ny)-ny) > mytol:
            ny = int(ny) + 1
        else:
            ny = int(ny)
        xx, yy = self._get_rect_positions(xstart, ystart, dx, dy, nx, ny, snake, centered)
        mask = ((xx-cx)**2 + (yy-cy)**2) < r**2
        xx = xx.ravel()
        yy = yy.ravel()
        positions = [ (xx[i], yy[i]) for i,b in enumerate(mask.ravel()) if b]
        self._add_pattern(pname, positions, patt_args, overwrite=overwrite)

    def _add_pattern(self, pname, positions, patt_args, overwrite=False):
        if self._last_index.get(pname) is None or overwrite:
            self._last_index[pname] = [-1, patt_args] 
            self._positions[pname] = positions
            self._counts[pname] = [0,]*len(positions)
            self._exposure_times[pname] = [0,]*len(positions)
        elif self._last_index[pname][1] != patt_args or self._positions[pname] != positions:
            raise ValueError(f"The pattern {pname} already exist with different arguments {self._last_index[pname][1]}\nRemove existing pattern first or use a different name.")
            
        

    def _get_rect_positions(self, xstart, ystart, dx, dy, nx, ny, snake=True, centered=False):
        xstop = xstart + nx*dx
        ystop = ystart + ny*dy
        if centered:
            endpoint = False
        else:
            endpoint = True
        x = np.linspace(xstart, xstop, int(nx)+1, endpoint=endpoint)
        y = np.linspace(ystart, ystop, int(ny)+1, endpoint=endpoint)
        if centered:
            x = x + dx/2
            y = y + dy/2    
        xx, yy = np.meshgrid(x, y)
        if snake and  xx.shape[0]>1:
            for i in range(1, xx.shape[0], 2):
                line = list(xx[i])
                line.reverse()
                xx[i] = line
        return xx.ravel(), yy.ravel()

    def remove_pattern(self, pname):
        if self._last_index.get(pname) is not None:
            #self._last_index[pname] = None #removes key:value in redis  (del works too)  
            try:        
                del self._last_index[pname] # because we know it is there
                self._positions.pop(pname, None)
                self._counts.pop(pname, None)
                self._exposure_times.pop(pname, None)
            except:
                print('pattern \'{:s}\' does not exist! Nothing removed.'.format(pname))
    
    def remove_all_patterns(self):
        all_pattern_names = self.get_pattern_list()
        for pname in all_pattern_names:
            print('removing pattern \'{:s}\''.format(pname))
            self.remove_pattern(pname)         
            
        
    def has_pattern(self, pname):
        if self._last_index.get(pname) is not None:
            return True
        return False
        
    def get_pattern_list(self):
        return list(self._last_index.keys())
        
    def get_pattern_params(self, pname):
        if pname not in self._last_index.keys():
            return None
        else:
            return self._last_index[pname][1]
        
    def show(self,experiment, display_indices = False):
        f = flint()
        p = f.get_plot(PositionManagerPlotInBliss,
               name="Position Manager Plot",
               unique_name="pm_plot",
               selected=True)
        p.clear_data()
        # plot each pattern but retain xmin/max, ymin/max
        xmin = np.nan
        xmax = np.nan
        ymin = np.nan
        ymax = np.nan
        for pix, pattern in enumerate(self.get_pattern_list()):
            x=[]
            y=[]
            count =[]
            x.extend([i for (i,j) in self.get_positions(pattern)])
            y.extend([j for (i,j) in self.get_positions(pattern)])
            one_count = self.get_cycle_counts(pattern)
            count.extend(one_count)
            xmin = np.nanmin([xmin, np.min(x)])
            xmax = np.nanmax([xmax, np.max(x)])
            ymin = np.nanmin([ymin, np.min(y)])
            ymax = np.nanmax([ymax, np.max(y)])
            pcolor = ecolors[np.remainder(pix,len(ecolors))]
            print(experiment['shg'])
            if experiment.get('shg'):
                shg = experiment['shg']
            else:
                shg = .1 #mm
            if experiment.get('theta'):
                stheta = experiment['stheta']
            else:
                stheta = 45 #deg
                
            width = shg/np.sin(np.deg2rad(stheta))
                
            p.add_pattern(x, y, count, pattern, pcolor, width = width, height = experiment['svg'], display_indices = display_indices)
        # prepare plot layout
        if experiment.get('sample_mot1'):
            p.set_xlabel(experiment['sample_mot1'])
        if experiment.get('sample_mot2'):
            p.set_ylabel(experiment['sample_mot2'])
        p.update_limits(xmin, xmax, ymin, ymax)
        p.finalize_plot()
        return p

    def select_plot_points(self, experiment, number_of_points = 1):
        # does not work!!!!
        p = self.show(experiment)
        points = p.select_points(number_of_points)
        return points

def test_position_manager():

    import matplotlib.pyplot as plt

    class Display:
        def __init__(self, interactive=True, dtmin=0.001, defsize=(800, 600)):
            self._interactive = interactive
            self._dtmin = dtmin

            if interactive:
                plt.ion()
            else:
                plt.ioff()

            self.plot = plt.imshow(np.zeros((defsize[1], defsize[0])))
            plt.pause(self._dtmin)

        def __del__(self):
            plt.close()
            plt.ioff()

        def show(self, arry):
            try:
                plt.cla()  # clear axes
                # plt.clf()   # clear figure
            except Exception:
                pass

            self.plot = plt.imshow(arry)
            if self._interactive:
                plt.pause(self._dtmin)
            else:
                plt.show()

        def close(self):
            plt.close()
            plt.ioff()

    pm = PositionManager("PositionManager")

    Nx, Ny = 50, 50
    nx, ny = Nx/2, Ny/4
    dx, dy = 1, 1
    xstart = 0 #Nx/10
    ystart = 0 #Ny/10
    cx, cy = 3*Nx/4, 3*Ny/4
    diameter = min(Nx, Ny)/2

    pm.add_rect_pattern('rect1', xstart, ystart, dx, dy, nx, ny, snake=True)
    pm.add_circular_pattern('circ1', cx, cy, dx, dy, diameter, snake=True)


    disp = Display(defsize=(Nx,Ny))
    rate = 4
    arry  = np.zeros((Ny,Nx), dtype='uint32')
    disp.show(arry)

    if 1:
        for i in range(2*len(pm.get_positions('rect1'))):
            x,y = pm.get_next_position('rect1')
            val = pm.get_cycle_count('rect1')
            arry[int(y),int(x)] = val
            if i%rate == 0:
                disp.show(arry)

    if 1:
        for i in range(len(pm.get_positions('circ1'))):
            x,y = pm.get_next_position('circ1')
            val = pm.get_cycle_count('circ1')
            arry[int(y),int(x)] = val
            if i%rate == 0:
                disp.show(arry)

    disp.show(arry)
