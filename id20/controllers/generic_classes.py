import functools
from bliss.controllers.motor import CalcController 
from bliss.physics.units import ur


def manage_units (func):
    @functools.wraps(func)
    def wrapper(obj, values_dict):
        obj._convert_to_internal (values_dict)
        ret_dict=func(obj, values_dict)
        obj._convert_to_user (ret_dict)
        return ret_dict
    return wrapper


class ID20CalcController (CalcController):
    def __init__(self, *args, **kargs):
        super().__init__(*args, **kargs)
        self.units = {}
        self._internal_units = {}

    def initialize(self):
        super().initialize()
        # get all motor units

        for tag in self._tagged:
            if tag !='real':
                self.units [tag] = self._internal_units [tag] if self._tagged[tag][0].unit is None else self._tagged[tag][0].unit
                if tag not in self._internal_units:
                    raise RuntimeError (f"Missing Controller internal units for role {tag}")
                
    def _convert_from_to (self, val, from_unit, to_unit):
        return (val*ur.parse_units(from_unit)).to(to_unit).magnitude
    
    def _convert_to_internal (self, val_dict):
        for tag in val_dict:
            val_dict[tag] = self._convert_from_to (val_dict[tag], self.units[tag], self._internal_units[tag])
        return val_dict
    
    def _convert_to_user (self, val_dict):
        for tag in val_dict:
            val_dict[tag] = self._convert_from_to (val_dict[tag], self._internal_units[tag], self.units[tag])
        return val_dict

    
