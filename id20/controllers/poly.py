"""
- class: Polynomial
  package: id20.controllers.poly
  plugin: generic
  toreal_coefs: 13.43271655609 0.312510420518003 -0.0400161535969228 0.0017655920520294  -0.0000400470702503 0.000000449234502 -0.0000000019859709 
  fromreal_coefs: 73.62203 1.02046 -0.27153 -0.03171 0.00179 -0.0000127025
  axes:
    - name: $simumot
      tags: real motor
    - name: iris
      tags: calc

"""

import numpy
from bliss.controllers.motor import CalcController

class Polynomial(CalcController):
    def __init__(self, config):
        super().__init__(config)

        self._poly1 = config.get("toreal_coefs")
        self._poly2 = config.get("fromreal_coefs", None)

        try:
            self._poly1.reverse()
            self._poly2.reverse()
        except AttributeError:
            print(f"coef parameter (toreal_coefs and fromreal_coefs) is missing in config or wrong - must be a list of values")
            raise

        
    def calc_from_real(self, reals_dict):

        _, pos = reals_dict.popitem()

        positions_dict = {
            "calc": numpy.polyval(self._poly2,pos),
        }
        
        return positions_dict

    
    def calc_to_real(self, positions_dict):

        _, pos = positions_dict.popitem()
        
        reals_dict = {
            'motor': numpy.polyval(self._poly1,pos),
            }

        return reals_dict

    
