'''
Controller:
- name: d72
  class: D72Scan
  package: id20.controllers.hydra
  plugin: bliss
  axes: 
  - vdtx2
  - vdtx3
  - vdtx4
  - vdtx5
  - vdtx6
  - vdtx7
  - vdtx8
  - vdtx9
  - vdtx10
  - vdtx11
  - vdtx12
  - vutx1
  - vutx2
  - vutx3
  - vutx4
  - vutx5
  - vutx6
  - vutx7
  - vutx8
  - vutx9
  - vutx10
  - vutx11
  - vutx12
  - vbtx1
  - vbtx2
  - vbtx3
  - vbtx4
  - vbtx5
  - vbtx6
  - vbtx7
  - vbtx8
  - vbtx9
  - vbtx10
  - vbtx11
  - vbtx12
  - hrtx1
  - hrtx2
  - hrtx3
  - hrtx4
  - hrtx5
  - hrtx6
  - hrtx7
  - hrtx8
  - hrtx9
  - hrtx10
  - hrtx11
  - hrtx12
  - hltx1
  - hltx2
  - hltx3
  - hltx4
  - hltx5
  - hltx6
  - hltx7
  - hltx8
  - hltx9
  - hltx10
  - hltx11
  - hltx12
  - hbtx1
  - hbtx2
  - hbtx3
  - hbtx4
  - hbtx5
  - hbtx6
  - hbtx7
  - hbtx8
  - hbtx9
  - hbtx10
  - hbtx11
  - hbtx12

'''

from bliss.common.axis import Axis
from bliss.common.scans import dnscan

from id20.helpers.position_instances import PositionInstances

class D72Scan:
    
    def __init__(self, name, config):
        self.name = config.get('name')
        self._config = config
        self._axes = config.get('axes')
        self._motors = dict()

        self.positions = PositionInstances (
            self.name,
            *self._axes,
        )

        for axis in self._axes:
            assert isinstance(axis, Axis), f"{axis} is not an Axis"
            self._motors [axis.name]=axis
            
    def scan (self, start, end, intervals, itime, *counters):
        motion = list(map(lambda x:(x,start,end),self._motors.values()))
        s=dnscan (motion, intervals, itime, *counters, run=False)
        s.scan_info.update({'title': f'd72scan {start} {end} {intervals} {itime}'})
        s.run()
            
    @property
    def motors (self):
        return self._motors


    def save_positions (self):
        self.positions.update()
        self.positions.parameters.to_beacon(f'{self.name}_{self.positions.name}',*self.positions.parameters.instances)
        
    def load_positions (self):
        self.positions.parameters.from_beacon(f'{self.name}_{self.positions.name}',self.positions.instance_name)
        
    def move_to_positions (self):
        self.positions.move_all()


     

        
#PositionInstance stuff        
#ok        def save(self): save current positions to a yml in beacon
#        def load(self): read in position saved last time or from a h5, then set|move|nothing
#        def set(self): set position from loaded register
#        def move(self): move to loaded register pos (keep original positions before moving)

    
