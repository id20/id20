"""
- class: ID20SGonio
  package: id20.controllers.sample_gonio
  plugin: generic
  d: 468
  axes:
    - name: $srty_eh2
      tags: real linear
      unit: mm
    - name: phi_eh2
      tags: angle
      unit: deg
- class: ID20SGonio
  package: id20.controllers.sample_gonio
  plugin: generic
  d: 565
  axes:
    - name: $srtx_eh2
      tags: real linear
      unit: mm
    - name: chi_eh2
      tags: angle
      unit: deg
"""

import numpy
from bliss.controllers.motor import CalcController
from bliss.physics.units import ur

D1=177

class ID20SGonio(CalcController):
    def __init__(self, config):
        super().__init__(config)
        self._d = (config.get("d")*ur.mm).magnitude


    def calc_from_real(self, reals_dict):
        D2 = self._d
        x = reals_dict['linear']
        
        b = numpy.sqrt (D2*D2 + D1*D1)
        a = x + D1

        beta  = numpy.arctan(D1/D2)
        alpha = numpy.arccos((-a*a+b*b+D2*D2)/(2*D2*b))
        gamma = alpha - beta

        pseudos_dict = {
            'angle': numpy.degrees(gamma),
        }
        
        return pseudos_dict

    def calc_to_real(self, pseudos_dict):
        D2 = self._d
        gamma = numpy.radians(pseudos_dict['angle'])
        D2 = D2
        b = numpy.sqrt (D2*D2 + D1*D1)
        beta = numpy.arctan(D1/D2)
        alpha = beta + gamma
        a = numpy.sqrt (b*b + D2*D2 -2*b*D2*numpy.cos(alpha))
        x = a - D1

        reals_dict = {
            'linear': x,
        }
        
        return reals_dict


