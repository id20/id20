import numpy

from bliss.controllers.spectrometers.spectro_base import Spectrometer, Detector, CylindricalAnalyser

class ID20Spectrometer (Spectrometer):

    def cross_offsets_on_detector (self):
        for analyser in self.analysers:
            analyser.offset_on_detector*=-1


class ID20Detector(Detector):

    def compute_bragg_solution(self, bragg):
        result = self.target._get_reals_pos(bragg)
        reals_pos = { 
            "xpos": result[0],
            "zpos": result[1],
            "pitch": result[2],
        }

        bragg_solution = {}
        
        return (bragg, bragg_solution, reals_pos)

    

    
class ID20Analyser(CylindricalAnalyser):

    def __init__ (self, *args, **kargs):
        super().__init__ (*args, **kargs)

        print ("Initialising",self.name)
        
        self.rc = RowlandCircle()
        
        
    def compute_bragg_solution(self, bragg):

        result = self._get_reals_pos(bragg)
        reals_pos = { 
            "rpos": result[5],
            "zpos": result[6],
            "pitch": result[3],
            "yaw": result[4],
        }

        bragg_solution = {}

        return (bragg, bragg_solution, reals_pos)


    def _get_reals_pos (self, _bragg_angle):
        
        def cart2sph (x,y,z):
            XsqPlusYsq = x**2 + y**2
            r = numpy.sqrt(XsqPlusYsq + z**2)              # r
            elev = numpy.arctan(z/numpy.sqrt(XsqPlusYsq))          # theta
            az = numpy.arctan(y/x)                           # phi
        
            return (az, elev, r)

        self.rc.rc_calc(
            _bragg_angle,
            self.radius,
            self.angular_offset,
            self.miscut,
            self.offset_on_detector,
        )
        
        spherical = cart2sph (-self.rc.rc_analyser_normal.T[0],
                                   -self.rc.rc_analyser_normal.T[1],
                                   self.rc.rc_analyser_normal.T[2])
        
        spherical = numpy.array(spherical)

        ath = numpy.degrees(numpy.pi/2-spherical[1])
        achi   = spherical[0]-self.rc.rc_alpha

#        self.achim  = map (lambda x  : 47.5*tan(x+atan(10.82/47.5))-10.82,  achi)
        achi   = numpy.degrees(achi)
        ax     = self.rc.rc_analyser.T[0]/numpy.cos(self.rc.rc_alpha)

#        beta = numpy.radians(self.beta)
        beta = numpy.radians(21)

        dx=self.rc.rc_detector[0]*numpy.cos(beta)-self.rc.rc_detector[2]*numpy.sin(beta)
        dz=self.rc.rc_detector[0]*numpy.sin(beta)+self.rc.rc_detector[2]*numpy.cos(beta)
        detnorm = self.rc.rc_det_0-self.rc.rc_center_0

        detnorm = detnorm/numpy.sqrt(detnorm[0]**2+detnorm[1]**2+detnorm[2]**2)

        drot = numpy.degrees(numpy.arccos (numpy.dot(detnorm,numpy.array(self.rc.X))))
#        drot = 2*ath [2]

        result = [dx,dz,drot]

        result.append (ath)
        result.append (achi)
        result.append (ax)
        result.append (self.rc.rc_analyser[2]) #az

        return result

    
#
# old lib src
#

class RowlandCircle:

    def __init__(self):

        self.rc_detector = numpy.ndarray([1,3])
        self.rc_analyser = numpy.ndarray([1,3])
        self.rc_rowland_center = numpy.ndarray([1,3])
        self.rc_analyser_normal = numpy.ndarray([1,3])

        
    def rc_calc (self, bragg_angle, analyser_radius, angular_offset, asymetry, detector_offset):
        
        def _rotation_matrix (versor, theta):
            
            #versor=versorr
            norm = numpy.sqrt(versor[0]**2+versor[1]**2+versor[2]**2)
            if norm != 1:
                versor[0]=versor[0]/norm
                versor[1]=versor[1]/norm
                versor[2]=versor[2]/norm
   
            a= [versor[0]**2+(1-versor[0]**2)*numpy.cos(theta), (1-numpy.cos(theta))*versor[0]*versor[1]-numpy.sin(theta)*versor[2], (1-numpy.cos(theta))*versor[0]*versor[2]+numpy.sin(theta)*versor[1]]
            b= [(1-numpy.cos(theta))*versor[0]*versor[1]+numpy.sin(theta)*versor[2], versor[1]**2+(1-versor[1]**2)*numpy.cos(theta), (1-numpy.cos(theta))*versor[1]*versor[2]-numpy.sin(theta)*versor[0]]
            c= [(1-numpy.cos(theta))*versor[0]*versor[2]-numpy.sin(theta)*versor[1], (1-numpy.cos(theta))*versor[1]*versor[2]+numpy.sin(theta)*versor[0], versor[2]**2+(1-versor[2]**2)*numpy.cos(theta)]

            return numpy.array ([a, b, c])

        #
        #
        #
        
        bragg_angle = numpy.radians(bragg_angle)

        self.rc_alpha = numpy.radians(angular_offset)
        asym = numpy.radians(asymetry)
        
        self.rc_ana_0 = numpy.array([
            2*analyser_radius*numpy.sin(bragg_angle-asym),
            0,
            0,
        ])

        self._rc_det_x = 2*analyser_radius*numpy.cos(bragg_angle+asym)*numpy.sin(2*bragg_angle)
        self._rc_det_y = 0
        self._rc_det_z = 2*analyser_radius*numpy.sin(bragg_angle+asym)*numpy.sin(2*bragg_angle)

        self.rc_det_0 = numpy.array([
            self._rc_det_x,
            self._rc_det_y,
            self._rc_det_z
        ])

        self.rc_center_0 = numpy.array([
            analyser_radius*numpy.sin(bragg_angle-asym),
            0,
            analyser_radius*numpy.cos(bragg_angle-asym),
        ])

        psi, eta = self._psieta_calc (detector_offset)

        self.X = X = [1, 0, 0]
        Y = [0, 1, 0]
        Z = [0, 0, 1]

        self.rc_detector = numpy.dot(_rotation_matrix (X, psi),self.rc_det_0.T)
        det = numpy.copy(self.rc_detector)

        self.rc_analyser = numpy.dot(_rotation_matrix (det,eta),self.rc_ana_0.T)
        self.rc_rowland_center = numpy.dot(_rotation_matrix(det,eta),numpy.dot(_rotation_matrix(X,psi),self.rc_center_0.T))
        self.rc_analyser_normal = -numpy.dot(_rotation_matrix(det,eta),numpy.dot(_rotation_matrix(X,psi),numpy.dot(_rotation_matrix(Y,numpy.pi/2-bragg_angle+asym),X)))
        self.rc_analyser_normal = self.rc_analyser_normal/numpy.sqrt(self.rc_analyser_normal[0]**2+self.rc_analyser_normal[0]**2+self.rc_analyser_normal[2]**2)


        
    def _psieta_calc (self, detector_offset):

        psi = -numpy.arcsin(detector_offset/self._rc_det_z)
                

        dx = self._rc_det_x
        dz = self._rc_det_z
        alpha = self.rc_alpha

        eta = (numpy.sin(psi)**2*dx**2*dz**2+numpy.sin(psi)*dx**3*dz*numpy.tan(alpha)-numpy.sin(psi)*dx*dz**3*numpy.tan(alpha)-dx**2*dz**2*numpy.tan(alpha)**2+abs(dz)*numpy.cos(psi)*(dx**2+dz**2)*numpy.sqrt(-2*numpy.sin(psi)*dx*dz*numpy.tan(alpha)-dx**2*numpy.tan(alpha)**2+dz**2*(numpy.cos(psi)**2+numpy.tan(alpha)**2)))/(dz**2*(dx**2-2*numpy.sin(psi)*dx*dz*numpy.tan(alpha)+dz**2*(numpy.cos(psi)**2+numpy.tan(alpha)**2)))

        eta = round(eta,15)
        eta = numpy.arccos(eta)
        eta*= numpy.sign(alpha)
        
        
        return psi,eta



        
    
class NotUsed(RowlandCircle):
            
    def analyserX_calc (self,eta,analysers_radii):
        x=2*analysers_radii*sin(_bragg_angle)*(cos(_bragg_angle)**2+cos(eta)*sin(_bragg_angle)**2)
        return x

    def analyserY_calc (self,psi,eta,analysers_radii):
        y=2*analysers_radii*sin(_bragg_angle)**2*(cos(psi)*sin(eta)+(-1+cos(eta))*cos(_bragg_angle)*sin(psi))
        return y

    def analyserZ_calc (self,psi,eta,analysers_radii):
        z=2*analysers_radii*sin(_bragg_angle)**2*(sin(psi)*sin(eta)-(-1+cos(eta))*cos(_bragg_angle)*cos(psi))
        return z

    def detectorX_calc (self,psi,analysers_radii):
        x=4*analysers_radii*cos(_bragg_angle)**2*sin(_bragg_angle)
        return x

    def detectorY_calc (self,psi,analysers_radii):
        y=-4*analysers_radii*cos(_bragg_angle)*sin(_bragg_angle)**2*sin(psi)
        return y

    def detectorZ_calc (self,psi,analysers_radii):
        z=4*analysers_radii*cos(_bragg_angle)*sin(_bragg_angle)**2*cos(psi)
        return z

    def rowlandCenterX_calc (self,eta,analysers_radii):
        rcx = analysers_radii*(cos(eta/2)**2*sin(_bragg_angle)+sin(eta/2)**2*sin(3*_bragg_angle))
        return rcx

    def rowlandCenterY_calc (self,psi,eta,analysers_radii):
        rcy = 0.5*analysers_radii*(-(cos(eta)*(cos(_bragg_angle)+cos(3*_bragg_angle))+4*cos(_bragg_angle)*sin(_bragg_angle)**2)*sin(psi)-2*cos(2*_bragg_angle)*cos(psi)*sin(eta))
        return rcy

    def rowlandCenterZ_calc (self,psi,eta,analysers_radii):
        rcz = 0.5*analysers_radii*((cos(eta)*(cos(_bragg_angle)+cos(3*_bragg_angle))+4*cos(_bragg_angle)*sin(_bragg_angle)**2)*cos(psi)-2*cos(2*_bragg_angle)*sin(psi)*sin(eta))
        return rcz

