"""
- class: ID20KB
  package: id20.controllers.kb
  plugin: generic
  name: kb3
  d3z: 419.5
  d3y: 136
  dz: 203
  axes:
    - name: $kbr3y
      tags: real roty
      unit: mrad
    - name: $kbt3z
      tags: real tz
      unit: mm
    - name: kbz
      tags: kbz
      unit: mm
    - name: kbroty
      tags: kbroty
      unit: mrad
"""

import numpy
from bliss.physics.units import ur
from id20.controllers.generic_classes import ID20CalcController, manage_units

class ID20KB(ID20CalcController):
    def __init__(self, config):
        super().__init__(config)

        self.dz = config.get("dz")
        self.d3z = config.get("d3z")
        self.d3y = config.get("d3y")

        self._internal_units = {
            'roty': 'mm',
            'tz': 'mm',
            'kbroty': 'rad',
            'kbz': 'mm',
            }

    @manage_units
    def calc_from_real(self, reals_dict):

        roty, tz = reals_dict["roty"], reals_dict["tz"]

        positions_dict = {
            "kbz": tz*self.dz/self.d3z,
            "kbroty": numpy.arctan(roty/self.d3y)-numpy.arctan(tz/self.d3z)
        }
        
        return positions_dict

    
    @manage_units
    def calc_to_real(self, positions_dict):

        kbrotx, kbz = positions_dict["kbroty"], positions_dict["kbz"]
        a = numpy.arctan(kbz/self.dz)+kbrotx
        roty = self.d3y*numpy.tan(a)

        reals_dict = {
            "tz": kbz*self.d3z/self.dz,
            "roty": roty,
            }

        return reals_dict
