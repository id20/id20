import functools
import shutil

from bliss.controllers.multiplepositions import MultiplePositions
from bliss.scanning.chain import ChainPreset, ChainIterationPreset
from bliss.common.logtools import log_debug, log_error, log_warning
from bliss.common.scans import DEFAULT_CHAIN
from bliss.common.user_status_info import status_message
from bliss.config.settings import SimpleSetting


class Attenuator(MultiplePositions):
    def __init__(self, name, config):
        super().__init__(name, config)

        self.simultaneous = False
        
        auto_preset = config.get("acquisition_presets", {})
        try:
            self._chain_preset_start = getattr(self, auto_preset.get("start"))
            self._chain_preset_stop = getattr(self, auto_preset.get("stop"))
        except:
            log_warning(self, "auto_preset start and stop not configured.")
            self._chain_preset_start = self._chain_preset_stop = None

        self._auto = SimpleSetting("attenuator_auto_mode", default_value=0)

        if self._auto.get():
            self.auto_on()

    def __info__(self):
        msg = super().__info__()
        msg += f" mode {self._mode()}\n"

        return msg



    def add_label_move_method(self, pos_label):

        from bliss import is_bliss_shell
        
        def label_move_func(mp_obj, pos):
            with status_message() as p:
                msg = f"Moving '{mp_obj.name}' to position: {pos}"
                siz = int(shutil.get_terminal_size().columns*0.9 - len(mp_obj.name) -30)
                p(msg.rjust(siz," "))
                mp_obj.move(pos)

        if pos_label.isidentifier():
            setattr(
                self,
                pos_label,
                functools.partial(label_move_func, mp_obj=self, pos=pos_label),
            )
        else:
            log_error(
                self, f"{self.name}: '{pos_label}' is not a valid python identifier."
            )

            
    def scan_metadata(self):
        mdata = super().scan_metadata()
        if mdata is None:
            mdata = dict()
        mdata.update(
            {
                "attenuator position": self.position,
                "attenuator mode": self._mode(),
            }
        )
        return mdata

    def _mode(self):
        if "attenuator_auto" in DEFAULT_CHAIN._presets:
            return "AUTO"
        else:
            return "MANUAL"

    def auto_on(self):
        if callable(self._chain_preset_start) and callable(self._chain_preset_stop):
            self._auto.set(1)
            DEFAULT_CHAIN.add_preset(
                AttenuatorChainIterationPreset(self), "attenuator_auto"
            )
        else:
            self.auto_off()

    def auto_off(self):
        self._auto.set(0)
        DEFAULT_CHAIN.remove_preset(name="attenuator_auto")


class AttenuatorChainIterationPreset(ChainPreset):
    def __init__(self, obj):
        ChainPreset.__init__(self)
        self._ctrl = obj

    class Iterator(ChainIterationPreset):
        def __init__(self, iteration_nb, obj):
            self.iteration = iteration_nb
            self._ctrl = obj

        def prepare(self):
            pass

        def start(self):
            log_debug(
                self._ctrl,
                "moving attenuation out of beam",
            )
            self._ctrl._chain_preset_start()

        def stop(self):
            log_debug(
                self._ctrl,
                "moving attenuation in beam",
            )
            self._ctrl._chain_preset_stop()

    def get_iterator(self, acq_chain):
        iteration_nb = 0
        while True:
            yield AttenuatorChainIterationPreset.Iterator(iteration_nb, self._ctrl)
            iteration_nb += 1
