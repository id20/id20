"""
- class: ID20Lin2Th
  package: id20.controllers.lin2th
  plugin: generic
  d: 47.5
  offset: -10.82
  axes:
    - name: $achim3_500
      tags: real linear
#      unit: mm
    - name: achi3_500
      tags: angle
      unit: deg

- class: ID20Lin2Th
  package: id20.controllers.lin2th
  plugin: generic
  d: 114
  axes:
    - name: $kbrt4z
      tags: real linear
      unit: mm
    - name: kbr4z
      tags: angle
      unit: mrad
"""

import numpy
from id20.controllers.generic_classes import ID20CalcController, manage_units
from bliss.physics.units import ur


class ID20Lin2Th(ID20CalcController):
    def __init__(self, *args, **kargs):
        super().__init__(*args, **kargs)
        self.d = self.config.get("d")
        self.lin_offset = self.config.get("offset",0.0)
        self._internal_units = {
            "linear": "mm",
            "angle": "rad",
        }


    @manage_units
    def calc_from_real(self, reals_dict):
        lin = reals_dict['linear']
        th = numpy.arctan((lin-self.lin_offset)/self.d)-numpy.arctan(-self.lin_offset/self.d)

        pseudos_dict = {
            'angle': th,
        }
        
        return pseudos_dict
    
    @manage_units
    def calc_to_real(self, pseudos_dict):
        th = pseudos_dict['angle']
        lin = self.d*numpy.tan(th+numpy.arctan(-self.lin_offset/self.d))+self.lin_offset

        reals_dict = {
            'linear': lin, 
        }
        
        return reals_dict



