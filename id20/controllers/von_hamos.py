"""
- controller:
  plugin: emotion
  package: id20.controllers.von_hamos
  class: VonHamos
  name: vonhamos
  description: Von Hamos Spectrometer Bragg
  radius: 250
  axes:
  - name: $az_hamos
    tags: real az
  - name: $dtz_hamos
    tags: real dz
  - name: bragg_hamos
    tags: bragg
    unit: degrees
"""

import numpy
from bliss.common.protocols import HasMetadataForScan, HasMetadataForDataset
from bliss.controllers.motor import CalcController
from bliss.physics.units import ur

#class VonHamos(CalcController, HasMetadataForScan, HasMetadataForDataset):
class VonHamos(CalcController):
    def __init__(self, *args, **kargs):
        CalcController.__init__(self, *args, **kargs)
        self.radius = (self.config.get("radius", default=250)*ur.mm).magnitude
        self._energy = self.config.get("energy_controller", default=None)
        
#    def dataset_metadata(self):
#        return meta_dict
#    def scan_metadata(self):
#        meta_dict = self.dataset_metadata()
#        meta_dict.pop("name")
#        meta_dict["@NX_class"] = "NXspectrometer"
#        meta_dict["radius"]
#        meta_dict["xtal"]
#        return meta_dict


#    def __info__(self):
# xtals - and all axis positions

    @property
    def xtal (self):
        return self._energy.controller._xtals if self._energy else None
    
    @xtal.setter
    def xtal (self, value):
         if self._energy :
             self._energy.controller._xtals.xtal_sel = value 
             
    def calculate (self, energy=None, bragg=None):
        result = {}
        if bragg:
            result.update(self._energy.controller.calc_from_real({'bragg': bragg}))
            result.update(self.calc_to_real({'bragg': bragg}))
        else:
            result.update(self._energy.controller.calc_to_real({'energy': energy}))
            result.update(self.calc_to_real({'bragg': result['bragg']}))

        return result            

        
    def calc_from_real(self, reals_dict):
        az = reals_dict['az']
        
        pseudos_dict = {
            'bragg': self._hamos_z2th (az)
        }
        
        return pseudos_dict

    def calc_to_real(self, pseudos_dict):
        bragg = pseudos_dict ['bragg']

        reals_dict = {
            'az': self._hamos_th2z (bragg),
            'dz': 2*self._hamos_th2z (bragg),
        }
        
        return reals_dict

    def _hamos_th2z (self, bragg):
        return self.radius/numpy.tan(numpy.radians(bragg))
            
    def _hamos_z2th (self, z):
        return numpy.degrees (numpy.arctan(self.radius/z))



class EnergyCalcMotor(CalcController):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._xtals = self.config.get("xtal_manager")

    def calc_from_real(self, real_positions):
        bragg = real_positions['bragg']
        if self._xtals.xtal_sel is not None:
            bragg_use = bragg * ur.Unit(self.reals[0].unit)
            bragg_deg = bragg_use.to("deg").magnitude
            energy_keV = self._xtals.bragg2energy(bragg_deg) * ur.keV
            energy_use = energy_keV.to(self.pseudos[0].unit)
            val =  energy_use.magnitude
        else:
            val = numpy.nan
        return {"energy": val}

    def calc_to_real(self, positions_dict):
        energy = positions_dict ["energy"]
        if (
            self._xtals.xtal_sel is not None
            and not numpy.isnan(energy).any()
        ):
            energy_use = energy * ur.Unit(self.pseudos[0].unit)
            energy_keV = energy_use.to("keV").magnitude
            bragg_deg = self._xtals.energy2bragg(energy_keV) * ur.deg
            bragg_use = bragg_deg.to(self.reals[0].unit)
            val = bragg_use.magnitude
        else:
            val = numpy.nan
        return {"bragg": val}
            




        
