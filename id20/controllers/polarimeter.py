#        LAMBDA = 2 * g_mo_d * sin(theta)
#        LAMBDA = hc / E
#        g_mo_d = asi / sqrt (H*H+K*K+L*L)
#        LAMBDA = 2 * asic / sqrt (H*H+K*K+L*L) * sin(thetacc)
#        LAMBDA = 2 * asi111 / sqrt (3) * sin(theta)


"""
def calc_pc1rz_from_pol_energy(pol_ene_value) '{
        th_PC1 = deg(asin(hc_over_e / ( 2 * pol_ene_value * d_spac_Si311 )))
        pc1rz_new_val = th_PC1 - PC1_asym
        return pc1rz_new_val
}'

pc1 xtal Si311 d-spacing ...

def calc_pc2rz_from_pol_energy(pol_ene_value) '{
        th_PC1 = deg(asin(hc_over_e / ( 2 * pol_ene_value * d_spac_Si311 )))
        th_PC2 = deg(asin(hc_over_e / ( 2 * pol_ene_value * d_spac_Si444 )))
        th_PC1_ref = deg(asin(hc_over_e / ( 2 * 11.215 * d_spac_Si311 )))
        pc2rz_new_val = 2*th_PC1 - 2*th_PC1_ref + th_PC2 - PC2_asym
        return pc2rz_new_val
}'



thrad=asin(hc_over_e / ( 2 * enepol * d ))
sin(thrad)=hc_over_e / ( 2 * enepol * d )
hc_over_e / ( 2 * enepol * d )=sin(thrad)
hc_over_e = ( 2 * enepol * d )*sin(thrad)
hc_over_e/enepol = 2 * d  * sin(thrad)
#         LAMBDA = 2 * g_mo_d * sin(theta)



- class: ID20Polarimeter
  package: id20.controllers.polarimeter
  plugin: generic
  name: polar
  axes:
    - name: $pc1rz
      tags: real pc1
      xtal: $pc1xtal
      asym: 0 #in degrees
      unit: degrees
    - name: $pc2rz
      tags: real pc2
      xtal: $pc2xtal
      asym: 0
      unit: degrees
    - name: polen
      tags: energy
      unit: KeV

- plugin: generic
  module: monochromator
  class: XtalManager
  name: pc1xtal
  xtals:
    - xtal: Si311

- plugin: generic
  module: monochromator
  class: XtalManager
  name: pc2xtal
  xtals:
    - xtal: Si444
#      dspacing:
"""

import numpy
from bliss.controllers.motor import CalcController


class ID20Polarimeter(CalcController):
    def __init__(self, config):
        super().__init__(config)

        self.xtals = dict()
        self.asym = dict()

        self.pc1_ref = config.get("pc1_ref")

        for axis in config["axes"]:
            if axis["tags"].startswith("real"):
                _, pc = axis["tags"].split()
                self.xtals[pc] = axis["xtal"]
                self.asym[pc] = axis["asym"]
                if len(axis["xtal"].xtal_names) == 1:
                    axis["xtal"].xtal_sel = axis["xtal"].xtal_names[0]

    def __info__(self):
        msg = ""
        for pc in self.xtals:
            msg += f"\n    {pc}: "
            msg += self.xtals[pc].__info__().replace("\n", "\n    ")
        return msg

    def _calc_pc1rz_from_pol_energy(self, pol_ene_value):
        return self.xtals["pc1"].energy2bragg(pol_ene_value) - self.asym["pc1"]

    def _calc_pc2rz_from_pol_energy(self, pol_ene_value):
        th_PC1 = self._calc_pc1rz_from_pol_energy(pol_ene_value)
        th_PC1_ref = self._calc_pc1rz_from_pol_energy(self.pc1_ref)
        th_PC2 = self.xtals["pc2"].energy2bragg(pol_ene_value)
        return 2 * th_PC1 - 2 * th_PC1_ref + th_PC2 - self.asym["pc2"]

    def _reverse_calc_pc1rz_from_pol_energy(self, pc1_th):
        return self.xtals["pc1"].bragg2energy(pc1_th - self.asym["pc1"])

    def _reverse_calc_pc2rz_from_pol_energy(self, pc1_th, pc2_th):
        th_PC1_ref = self._calc_pc1rz_from_pol_energy(self.pc1_ref)
        return self.xtals["pc2"].bragg2energy(
            pc2_th - 2 * pc1_th + 2 * th_PC1_ref + self.asym["pc2"]
        )

    def calc_from_real(self, reals_dict):
        pc1_th = reals_dict["pc1"]
        pc2_th = reals_dict["pc2"]
        ene1 = self._reverse_calc_pc1rz_from_pol_energy(pc1_th)
        ene2 = self._reverse_calc_pc2rz_from_pol_energy(pc1_th, pc2_th)
        if abs(ene1 - ene2) / (ene1 + ene2) < 0.1:
            return {"energy": ene1}
        else:
            print(ene1 / ene2)
            return {"energy": numpy.nan}

    def calc_to_real(self, positions_dict):
        reals_dict = dict()
        reals_dict["pc1"] = self._calc_pc1rz_from_pol_energy(positions_dict["energy"])
        reals_dict["pc2"] = self._calc_pc2rz_from_pol_energy(positions_dict["energy"])
        return reals_dict
