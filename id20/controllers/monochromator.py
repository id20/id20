import numpy
import tabulate

from scipy.constants import Planck, speed_of_light, elementary_charge, pi, electron_mass
        
# from bliss.common.protocols import HasMetadataForDataset, HasMetadataForScan
from bliss.common.utils import BLUE, ORANGE
from bliss.common.logtools import log_warning

from bliss.controllers.monochromator.monochromator import Monochromator
from bliss.controllers.monochromator.calcmotor import EnergyTrackerCalcMotor
from bliss.controllers.monochromator.tracker import EnergyTrackingObject

from id20.helpers.monochromator_tracker_selector import TrackerSelector

# from bliss.physics.units import ur


HC_OVER_E = Planck*speed_of_light/elementary_charge*1e7



"""
mode=0 # rmth or energy_rmth
mode=1 # t2z.tracking.on(); postmono.off()
mode=2 # postmono.channel_cut()
mode=3 # postmono.back_scattering()
mode=4 # postmono.4bounces()
beamheight=15    # fixed output beam height after monochromators.
cdist=8          # distance between postmono channel cut crystals
delta=0           # adjustment offset on beamheight in mode 2.
gamma=0           # adjustment offset on T2Z position in mode 2.
zcorrection= 0.8857 -0.3227 0.0399 -0.002 0.000034419  # adjust offset on T2Z
2thcorrection= 0                                       # adjust offset on R2Th
cH=3             # postmono channel cut crystals miller indice H
cK=1             # postmono channel cut crystals miller indice K
cL=1             # postmono channel cut crystals miller indice L
asipremono : premono crystal asi value if different from setmono.
asic      : postmono asi, specify if different from asipremono
"""


class ID20Monochromator(Monochromator):
    def __init__(self, config):

        super().__init__(config)

        self.postmono = config.get("postmono")  # for accessing the setup
        self.postmono._mono = self
        self.postmono._init2()
        
        self.u26tracking = config.get("u26tracking")  # for accessing the setup
        self.u26tracking._mono = self
        self.u26tracking._init2()

            

    # use motor offset on ID20
    @property
    def bragg_offset(self):
        return self.motors.bragg.offset

    def _get_info_xtals(self):
        info_str = super()._get_info_xtals()
        info_str += self.postmono.__info__()
        return info_str

    def _get_info_motor_tracking(self, energy=None):
        info_str = ""
        if hasattr(self, "tracking"):
            controller = self._motors["energy_tracker"].controller
            # TITLE ROW
            title = ["    ", ""]
            for axis in controller.pseudos:
                title.append(BLUE(axis.name))
            for axis in controller.reals:
                title.append(ORANGE(axis.name))
            if energy:
                title.append("bragg")
            # CALCULATED POSITION ROW
            bragg_pos = (
                self._motors["bragg_real"].position
                if energy is None
                else self.energy2bragg(energy)
            )
            energy_pos = self.bragg2energy(bragg_pos) if energy is None else energy
            calculated = ["    ", "Calculated"]
            valu = self._motors["energy"].unit
            calculated.append(f"{energy_pos:.5f} {valu}")
            for axis in controller.reals:
                if controller._axis_tag(axis) == "energy":
                    calculated.append(f"{energy_pos:.5f} {valu}")
                else:
                    target = axis.tracking.energy2tracker(energy_pos)
                    calculated.append(f"{target:.3f} {axis.unit}")
            if energy:
                calculated.append(f"{bragg_pos:.3f} deg")
            # CURRENT POSITION ROW
            current = ["    ", "   Current"]
            axis = controller.pseudos[0]
            current.append(f"{axis.position:.5f} {axis.unit}")
            for axis in controller.reals:
                if controller._axis_tag(axis) == "energy":
                    current.append(f"{axis.position:.5f} {axis.unit}")
                else:
                    current.append(f"{axis.position:.3f} {axis.unit}")
            # TRACKING STATE ROW
            tracking = ["    ", "Tracking", "", ""]
            for axis in controller.reals:
                if controller._axis_tag(axis) != "energy":
                    if axis.tracking.state:
                        tracking.append("ON")
                    else:
                        tracking.append("OFF")
            
            info_str = tabulate.tabulate(
                [calculated, current, tracking],
                headers=title,
                tablefmt="plain",
                stralign="right",
            )
        return info_str

    #
    # ID20
    #

    def calc(self, energy):

        print(f"\n{self._get_info_mono()}")
        print(f"\n{self._get_info_xtals()}")
        print(f"\n{self._get_info_motor_energy()}")
        msg = self._get_info_motor_tracking(energy)
        print(f"\n\n{msg}\n")

    @property
    def xtal(self):
        return self._xtals

    @xtal.setter
    def xtal(self, xtal_name):
        self._xtals.xtal_sel = xtal_name


class ID20EnergyTrackerCalcMotor(EnergyTrackerCalcMotor):

    # use motor offset on ID20
    def energy_dial(self, energy_user):
        return energy_user


class ID20EnergyTrackers(EnergyTrackingObject):
    def __init__(self, config):
        super().__init__(config)
        self.beamheight = config.get("beamheight", 15.0)
        self.name = self._name

    def _get_track_from_theory(self, axis, param_id, energy):
        if self._parameters[axis.name]["param_id"][param_id]["theory"] is not None:
            func_name = self._parameters[axis.name]["param_id"][param_id]["theory"][
                "energy2tracker"
            ]
            meth = getattr(self, func_name)
            ene = numpy.copy(numpy.array(energy))
            if func_name.endswith("correction"):
                tracker = meth(
                    ene,
                    self._parameters[axis.name]["param_id"][param_id]["theory"],
                    axis,
                )
            else:
                tracker = meth(
                    ene, self._parameters[axis.name]["param_id"][param_id]["theory"]
                )

            return tracker
        else:
            raise RuntimeError("No method given to calculate tracker in Theory mode")

    def _energy2tracker(self, axis, energy):
        track = numpy.nan
        if axis in self._motors.values():
            selected_id = axis.tracking.param_id.get()
            selected_mode = axis.tracking.mode.get()
            if selected_id is not None and selected_mode is not None:
                if selected_mode == "polynom":
                    track = self._get_track_from_polynom(axis.name, selected_id, energy)
                if selected_mode == "table":
                    track = self._get_track_from_table(axis.name, selected_id, energy)
                if selected_mode == "theory":
                    track = self._get_track_from_theory(axis, selected_id, energy)
        return track

    #
    # ID20
    #

    def _ene2ene(self, energy, pars):
        if "erange" in pars:
            r = pars["erange"]
            if energy < r[0] or energy > r[1]:
                return numpy.nan
        return energy

    def _polynom(self, energy, pars, coefs_key="polynom"):
        if "erange" in pars:
            r = pars["erange"]
            if energy < r[0] or energy > r[1]:
                return numpy.nan
        param = pars[coefs_key]
        coefs = param.raw_list.copy()
        coefs.reverse()
        return numpy.polyval(coefs, energy)

    def _ene2premod(self, energy, pars):
        zcorr = self._polynom(energy, pars, "zcorrection")
        theta = self._mono.energy2bragg(energy)
        premod = (self.beamheight - zcorr) / (2.0 * numpy.cos(numpy.radians(theta)))
        return premod

    def _ene2premod_posmoth(self, energy, pars):
        if (
            self._mono.postmono._xtals is None
            or self._mono.postmono._xtals.xtal_sel is None
        ):
            # TODO add a way to warn user
            return numpy.nan
        posth = numpy.radians(self._ene2posmoth(energy))
        theta = numpy.radians(self._mono.energy2bragg(energy))
        xtal_sel = self._mono.postmono._xtals.xtal_sel
        cdist = self._mono.postmono._xtals.get_xtals_config("cdist")[xtal_sel]

        premod = (
            self.beamheight
            + pars["delta"]
            + cdist * numpy.sin(2 * posth) / numpy.sin(posth - pars["calpha"])
        ) / (2 * numpy.cos(theta)) + pars["gamma"]

        return premod

    def _ene2posmoth(self, energy, pars=None):
        if (
            self._mono.postmono._xtals is None
            or self._mono.postmono._xtals.xtal_sel is None
        ):
            # TODO add a way to warn user
            return numpy.nan
        return self._mono.postmono._xtals.energy2bragg(energy)

    def _ene2premono_2th_correction(self, energy, pars, axis):
        return axis.position + self._polynom(energy, pars, "2thcorrection")

    def _ene2ppth(self, energy, pars):
        dspacing = pars["dspacing"]
        delta = pars["delta"]

        return numpy.degrees(numpy.arcsin(dspacing / energy)) + delta


    def _ene2gap (self, energy, pars):
       
        wavelength = HC_OVER_E/energy
        K = elementary_charge / (2*pi*electron_mass*speed_of_light)*1e-3*pars["TB0"]*pars["TL0"]
        k = numpy.sqrt(4e-7*pars["harmonic"]*wavelength/pars["TL0"]*pars["TGAM"]**2-2)
        gap = pars["TG0"]-pars["TL0"]/pi*numpy.log(k/K)

        return gap

    def _gap2ene (self, gap, pars):
    
        K = codata.elementary_charge / (2*pi*codata.electron_mass*codata.speed_of_light)*1e-3*pars["TB0"]*pars["TL0"]
        wavelength = pars["TL0"]/(4e-7*pars["harmonic"]*pars["TGAM"]**2)*(K**2*numpy.exp(2*pi*(pars["TG0"]-gap)/pars["TL0"])+2)
        ene = HC_OVER_E/wavelength

        return ene 



class ID20PostMonochromator(TrackerSelector):
    def __init__(self, config):
        super().__init__(config)

        self._xtals = None
        
    def __info__(self):
        msg = super().__info__()
        if self._xtals:
            msg += "\n    "
            msg += self._xtals.__info__().replace("\n", "\n    ")
        return msg

    def _set_custom_keys(self, mode, key, item):
        if key == "xtals":
            self._xtals = item["xtals"]

            if self._xtals.xtal_sel is None and len(self._xtals.xtal_names) == 1:
                self.xtal = self._xtals.xtal_names[0]

    @property
    def xtal(self):
        return self._xtals

    @xtal.setter
    def xtal(self, xtal_name):
        self._xtals.xtal_sel = xtal_name
