import functools
from bliss.config import settings
from bliss.config.plugins.generic import ConfigItemContainer


class TrackerSelector(ConfigItemContainer):
    def __init__(self, config):
        super().__init__(config)

        self._motors = []
        self._mode = settings.SimpleSetting(
            f"{self._name}_selector_mode", default_value="off"
        )
        self._info_title = self.config.get("title") if "title" in self.config else name

    def _init2(self):
        # to be called from main mono initialisation for each selector
        # but _init() is automatically called too much in advance (no ._mono yet) by generic plugin

        for mode in self.config["modes"]:
            desc_list = self.config["modes"][mode]
            for item in desc_list:
                for key in item:
                    if key == "tracked":
                        if item[key] not in self._motors:
                            self._motors.append(item[key])

            setattr(self, mode, functools.partial(self.set, mode))

        self.set(self._mode.get())

    def __info__(self):
        modes = "/".join(self.config["modes"].keys())
        msg = f"\n\n    {self._info_title}: {self._mode.get()} ({modes})\n"

        return msg

    def set(self, mode):

        if mode not in self.config["modes"]:
            raise ValueError(f"{mode} not configured.")

        motors = self._motors.copy()

        self._mono.tracking.all_param_id(mode)
        self._xtals = None

        for item in self.config["modes"][mode]:
            for key in item:
                if key == "tracked":
                    item["tracked"].tracking.on()
                    motors.remove(item["tracked"])
                if key == "param_id":
                    self._mono.tracking.all_param_id(item["param_id"])
                if key == "mode":
                    self._mono.tracking.all_mode(item["mode"])

                self._set_custom_keys(mode, key, item)

        for axis in motors:
            axis.tracking.off()

        self._mode.set(mode)

    def _set_custom_keys(self, mode, key, item):
        pass
