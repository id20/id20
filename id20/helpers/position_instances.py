from bliss.common.axis import Axis
from bliss.common.motor_group import Group
from bliss.shell.cli.user_dialog import UserChoice
from bliss.shell.cli.pt_widgets import BlissDialog, button_dialog
from bliss.config.settings import scan
from id20.helpers.beamline_parameters import BeamlineParametersBase, switch_instance


def list_wardr_positions ():
    for item in scan(f"parameters:positions*"):
        if item.endswith(':creation_order'):
            print(item.split(':creation_order')[0])

def admin_wardr_positions ():
    return PositionInstances('default')
    

            
class BeamlinePositionsBase(BeamlineParametersBase):
    def __init__(self, *axis_list):
        positions_dict = {}
        self.axis_objlist = {}

        if len(axis_list) is 0:
            print(f"axis list is empty ...")            

        for axis in axis_list:
            if isinstance(axis, Axis):
                positions_dict[axis.name] = None
                self.axis_objlist[axis.name] = axis
            else:
                print(f"{axis} not an Axis in current session.")
                print("hint: do not use string names for axis list, but objects.")

#        print(f"initializing ...")
        super().__init__("positions", positions_dict)
#        print(f"axis names: {list(map(lambda x: x, positions_dict))}")
#        print(f"done. {len(self.axis_list())} axis slots by {len(self.parameters.instances)} instances.")


    def __dir__(self):
        return ('setup','update','position','move','move_all','show','add','remove','position_list')


    def _purge(self):
        """                                
        remove all existing instances of current saved positions from redis .
        make sure that' s what you want before proceeding ... no recovery possible                               
        """
        self.parameters.purge()


    def axis_list(self):
        return list(self.parameters.to_dict().keys())
    
        
    @switch_instance
    def position_list(self):
        return dict(self.parameters.to_dict())

    
    @switch_instance
    def update(self, *axis_list):
        """
        update instance to current positions 
        """
        parameters_dict = self.parameters.to_dict(export_properties=True)
        ret = {}
        axis_list = self.axis_objlist.values() if len(axis_list) == 0 else axis_list
        for axis in axis_list:
            if isinstance(axis, Axis) and axis.name in parameters_dict:
                ret[axis.name] = axis.position
        print(f"updating {self.instance_name} with {ret}")

        parameters_dict.update(ret)
        self.parameters.from_dict(parameters_dict)

    @switch_instance
    def move(self, *axis_list, wait=True):
        motion = []
        positions_dict = self.parameters.to_dict()
        for axis in axis_list:
            if not isinstance(axis, Axis):
               axis_list.remove(axis)
            elif axis.name not in positions_dict:
               axis_list.remove(axis)
            elif positions_dict[axis.name] is None:
               axis_list.remove(axis)
            else:
               motion.append (axis)
               motion.append(positions_dict[axis.name])

        if len(axis_list) is 0:
            print("You must specify the list of axis you want to move, or use .move_all()")
            print("nothing to move...")
        else:
            motors_group = Group(*axis_list)
            motors_group.move (*motion, wait=wait)

            
    @switch_instance
    def move_all(self, wait=True):
        """
        move all motors in instance to instance positions
        """
        motion = []
        axis_list = []
        positions_dict = self.parameters.to_dict(export_properties=False)
        for name in positions_dict:
            axis = self.axis_objlist[name]
            if positions_dict[name] is not None:
                axis_list.append(axis)
                motion.append(axis)
                motion.append(positions_dict[name])
        if len(motion):
            mots = Group(*axis_list)
            mots.move(*motion, wait=wait)
        else:
            print("nothing to move...")


    @switch_instance
    def position(self, axis, new_position=None):
        """
        assign a position to one particular axis 
        can also be done with setup() in an interactive way
        """
        parameters_dict = self.parameters.to_dict(export_properties=True)
        ret = {}
        if isinstance(axis, Axis) and axis.name in parameters_dict:
            if new_position is not None:
                ret[axis.name] = float(new_position)
                parameters_dict.update(ret)
                self.parameters.from_dict(parameters_dict)
            else:
                return parameters_dict[axis.name] 
        else:
            print(f"{axis.name if isinstance(axis, Axis) else axis} not an axis in the saved positions list, sorry.")
            

    @switch_instance
    def add(self, *axis_list):
        """
        add new axis to the set 
        """
        for axis in axis_list:
            if isinstance(axis, Axis):
                self.parameters.add(axis.name, None)
                self.axis_objlist.update({axis.name: axis})
                print(f"{axis.name} added")
            else:
                print(f"{axis} not an axis")
        if len(axis_list) is 0:
            print("Nothing added, please specify axis you want to add")

    @switch_instance
    def remove(self, *axis_list):
        """
        remove axis from the set
        """
        for axis in axis_list:
            if isinstance(axis, Axis):
                self.parameters.remove("." + axis.name)
                self.axis_objlist.pop(axis.name)
                print(f"{axis.name} removed")
            else:
                print(f"{axis} not an axis")
        if len(axis_list) is 0:
            print(
                "Nothing removed, please specify axis you want to be removed from the instances"
            )

            
    @switch_instance
    def remove_instance(self, instance_name):
        """
        remove instance 
        """
        if instance_name in self.parameters.instances:
            self.parameters.remove(instance_name)
            print(f"{instance_name} removed.")
        else:
            print(f"{instance_name} unknown instance ")

            
            
class PositionInstances(BeamlinePositionsBase):
    """

    Class provinding tools to store/retrieve positions of a set of axis along other user actions.
    the positions are made persistant in time, so that they can be retrieved even in case of quitting bliss session.
    they belong to one particular bliss session and cannot be shared between sessions.

    Usage:

    - **user_instance = PositionInstances ('instance_name' , mot1, mot2, mot3 ...)**
      loads as 'user_instance' session object any previously initialised PositionInstances with the name 'instance_name', or create it if it is the first time it is instantiated.
       - if a list of axis is passed as arguments, slots are prepared for that set of axis in redis database. 
       - if no axis is passed, slots are prepared for all the motors of the session.
  
    - **user_instance.update ()**
      stores in 'user_instance' object (attached to 'instance_name' in redis) the current positions of the axis in the set. 
    - **user_instance.update  (mot1, mot2)**
      stores in 'user_instance' object the current positions of the listed axis only. 
    - **user_instance.assign  (mot3, 3.21)**
      stores in 'user_instance' the specified position value for the specified axis. value can be None.
    - **user_instance.setup ()**
      launches a dialog window to enter positions to be stored in 'user_instance'. Position can be None 
    - **user_instance.move (mot3, mot4)**
      move all axis listed to the position that was stored (if any) for them in 'user_instance'
    - **user_instance.move_all ()**
      move all axis of the set to the position that was stored (if any) for them in 'user_instance'. 
    - **user_instance.load (directory, suffix='')**
      stores positions read from a previously saved file (see .save() below)
      - directory (path) must be specified
      - a suffix can be given, the filename is : {session}_positions{suffix}.yml 

    The following functions act on all the position instances existing in redis database. The behaviour will be the same whatever is the session object calling it.
    - **user_instance.show ()**
      shows all positions sets stored among their different existing instances in redis database. It might happen that some instances are no longer useful. **note that this is the user responsibility to clear unused instances** to free redis memory with the next command.
    - **user_instance.remove_instance ('unwanted_instance_name')**
      remove specified instance from the database.

    - **user_instance.add  (mot3, mot4, mot5)**
      add some axis slots. do not forget to assign them some positions after. (.update(), .assign() or .setup()). 
    - **user_instance.remove (mot1, mot2)**
      remove axis slots from the instances.
  
    - **user_instance.save (directory, suffix = '')**
      saves to disk a yml files with all instances content.
      - a directory must be specified.
      - a suffix can be given,
      - the filename is : {session}_positions{suffix}.yml 


    Example:

       pos0 = PositionsInstances('oldpos0', gap)
       pos0.update()
       #stores gap position

       user_script_aligning_mot1() 

       pos1 = PositionsInstances('oldpos1', mot1)
       #create a slot for mot1, gap slot is kept.  
       pos1.update()
       #stores new positions for gap and mot1 

       pos0.move(gap) 
       #moves back gap to pos0 stored position 

       user_script_aligning_continue() 

    """

    def __init__(self, instance_name, *axis):
        print(f"Initialising {instance_name}")
        super().__init__(*axis)
        self.instance_name = instance_name



