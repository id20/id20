import os

from bliss.common.standard import info
from bliss.config.wardrobe import ParametersWardrobe
from bliss.config.settings import QueueSetting, OrderedHashSetting, scan
from bliss.shell.cli.user_dialog import UserChoice, UserInput, UserIntInput, UserFloatInput
from bliss.shell.cli.pt_widgets import BlissDialog


def check_name (func):
    def wrapper (name):
        bli = name.split(':')
        if name.startswith('parameters:'):
            name = bli[0] +':'+ bli[1]
        else:
            name = 'parameters:' + bli[0]

        return func (name)
    return wrapper

@check_name
def purge_wardr (name):
    instances = QueueSetting(name)
    inst_names = instances.get()

    for instance in inst_names:
        pr = OrderedHashSetting("%s:%s" % (name, instance))
        pr.clear()
        instances.remove(instance)  # removing from Queue
    
    instances.clear()

@check_name    
def read_wardr (name):
    instances = QueueSetting(name)
    inst_names = instances.get()

    for instance in inst_names:
        print (instance)
        pr = OrderedHashSetting("%s:%s" % (name, instance))
        for k in pr.keys():
            print (k,pr[k])

def list_wardr (wildchared = '*'):
    for item in scan(f'parameters:{wildchared}'):
        print(item)
        

def list_wardr_parameters ():
    for item in scan("parameters:*beamlineparams_*"):
        print(item)

        
def switch_instance (func):
    def wrapper (*args, **kwargs):
        if args[0].instance_name:
            args[0].parameters.switch(args[0].instance_name)
        return func (*args, **kwargs)
    return wrapper


def _dialog (title, parameters_dict, callbacks ={}):
    uilist = []
    for param in parameters_dict:

        validator = parameters_dict [param]
        if isinstance (validator, int):
            uilist.append([UserIntInput (param , param + ' : ' ,parameters_dict[param])])
        elif isinstance(validator, float):
            uilist.append([UserFloatInput (param , param + ' : ' ,parameters_dict[param])])
        else:
            uilist.append([UserInput (param , param + ' : ' ,parameters_dict[param])])
               
    ret = BlissDialog( uilist, title=title).show()

    if ret is not False:
            
        for param in parameters_dict:
            if isinstance(parameters_dict[param], list):
                ret[param]=eval(ret[param])
        for param in callbacks:
            if param in ret:
                if ret[param] != parameters_dict[param]:
                    ret[param] = callbacks[param].__call__(ret[param])
            else:
                ret[param] = callbacks[param].__call__(ret)
                
    return ret 



class BeamlineParametersBase:
    """
    Base class to handle beamline persistant parameters.
    """

    def __init__(self, name, param_defaults=None, no_setup=[], callbacks = {}):
        """
        Reads the parameter set from Redis and add the default values when necessary
        """

        assert name,'a name is required'
        
        self.name = name
        self.instance_name = None
        self._param_setup = {}
        self._callbackss = callbacks


        if self.name != None:
            self.parameters = ParametersWardrobe(self.name)

        if param_defaults != None:
            for k,v in param_defaults.items():
                    nosetup = (k in no_setup)
                    callback = callbacks [k] if k in callbacks else None
                    self._add(k, v, nosetup = nosetup, callback = callback)


    @switch_instance
    def __info__ (self):
        string = info(self.parameters)
        string = self.name + '  ' + string if self.instance_name is None else string
        return string
            
    def _add (self, key, value, nosetup = False, callback = None):

        if nosetup is True and key in self._param_setup:
            self._param_setup.pop(self._param_setup.index(key))
            
        if nosetup is False and key not in self._param_setup:
            self._param_setup[key]=value
            
        if key not in self.parameters.to_dict(export_properties=True):
            self.parameters.add (key, value)
        
        if callable(callback):
            self._callbackss[key]=callback
        else:
            if key in self._callbackss:
                self._callbackss.pop(key)
           
        
    @switch_instance
    def show(self):
        if self.name != None:
            self.parameters.show_table()
      
            
    @switch_instance
    def setup(self):
        if self.name is None:
            return

        title = self.instance_name + ' Setup' if self.instance_name else self.name + ' Setup'
        parameters_dict = self.parameters.to_dict(export_properties=True)
        ret = _dialog (title, self._param_setup, self._callbackss)

        if ret is not False:
            parameters_dict.update(ret)
            self.parameters.from_dict(parameters_dict)

    '''
    @switch_instance
    def save (self, path):
        """
        save in a file current instance of a parameter set         
        """
        path = os.path.abspath(path)
        if self.name != None and os.path.exists(path):
            file_name = path + '/parameters_' + self.name +'.yml'
            self.parameters.to_file(file_name)
            print (f'Saving {self.parameters.current_instance} instance of {self.name} on: {file_name}')

    @switch_instance
    def load(self, path):
        """
        loads from a previously saved file 
        loads only instance referenced by the calling object. 
        """
        path = os.path.abspath(path)
        if self.name != None and os.path.exists(path):
            file_name = path + '/parameters_' + self.name +'.yml'
            self.parameters.from_file (file_name, instance_name = self.parameters.current_instance) 
    '''
    @switch_instance
    def copy (self, instance_name):
        """
        """
        if instance_name in self.parameters.instances:
            instances = self.parameters._get_all_instances()
            copy_dict = instances [instance_name]
            self.parameters.from_dict(copy_dict)
        else:
            print (f"Can't find instance '{self.instance_name}' in parameters set '{self.name}'")
           
            

    def _reset_parameters(self):
        """
        Deletes all parameters of the object from Redis
        to re-start with the default values
        """
        if self.name != None:
            parameters_dict = self.parameters.to_dict()
            for k in parameters_dict.items():
                self.parameters.remove("."+k[0])

        self._param_setup.clear()        
        self._callbackss.clear()        

        self.__init__(self.name)        


class BeamlineParametersInstances(BeamlineParametersBase):

        def __init__(self, name, instance_name, defaults ={}):
            print("Initialising instance {0} of {1} parameters set ".format(instance_name, name))
            name = "beamlineparams_" + name
            super().__init__(name, defaults)
            self.instance_name = instance_name

            
        
