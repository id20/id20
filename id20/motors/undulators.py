import functools
import gevent
from bliss.common import tango
from bliss.common.axis import NoSettingsAxis
from bliss.common.hook import MotionHook
from bliss.common.logtools import log_debug, log_warning
from bliss.controllers.motors.esrf_undulator import ESRF_Undulator
from bliss.controllers.motors import esrf_undulator 

class UnduAxis(esrf_undulator.Axis):
    def __init__(self, name, controller, config):
         super().__init__(name, controller, config)
         self.settings.disable_cache('offset',False)

         
    @property
    def limits(self):
        return (self.low_limit, self.high_limit)

    @limits.setter
    def limits(self, limits):
        return self.limits

    @property
    def low_limit(self):
        # Return Low Limit in USER units.
        limit = self.config.get("low_limit", float, float("-inf"))
        return self.dial2user(limit)

    @low_limit.setter
    def low_limit(self, limit):
        return self.low_limit

    @property
    def high_limit(self):
        # Return High Limit in USER units.
        limit = self.config.get("high_limit", float, float("+inf"))
        return self.dial2user(limit)

    @high_limit.setter
    def high_limit(self, limit):
        return self.high_limit

Axis = UnduAxis


def _log_devfailed (func):
    @functools.wraps(func)
    def wrapper(*a, **b):
        try:
            return func(*a, **b)
        except tango.DevFailed as e:
            log_warning(
                func,
                "Undulator tango.DevFailed (Timeout? when executing {0} ({1}) ".format(
                    func.__name__,
                    e,
                )
            )
            raise e

    return wrapper


class Undulator(ESRF_Undulator):

    def __init__(self, *a, **ka):
        super().__init__(*a, **ka)
        self.machinfo = self.config.get('machinfo')

    @_log_devfailed
    def _set_attribute(self, *args):
        try:
            return ESRF_Undulator._set_attribute (self, *args)
        except tango.DevFailed as e:
            log_warning(
                self,
                "Undulator tango.DevFailed (Timeout? when executing ({0}) - will retry once".format(
                    e,
                ))
            gevent.sleep(0.1)
            return ESRF_Undulator._set_attribute (self, *args)

    @_log_devfailed
    def _get_attribute(self, *args):
        return ESRF_Undulator._get_attribute (self, *args)

    @_log_devfailed
    def read_position(self, axis):
        return ESRF_Undulator.read_position (self, axis)

    @_log_devfailed
    def state(self, axis):
        return ESRF_Undulator.state (self, axis)

    @_log_devfailed
    def stop(self, axis):
        return ESRF_Undulator.stop(self, axis)

    @_log_devfailed
    def stop_all(self, *motion_list):
        return ESRF_Undulator.stop_all(self, *motion_list)

    def steps_position_precision(self, axis):
        return axis.tolerance



class CheckForRefillHook(MotionHook):

    def __init__(self, name, config):
        self.config = config
        self.name = name
        super(CheckForRefillHook, self).__init__()

    def pre_move(self, motion_list):
        check_time = 0
        for motion in motion_list:
            val  = abs((motion.target_pos-motion.axis.position)/motion.axis.velocity)
            check_time = val if val > check_time else check_time
            
        for status in motion_list[0].axis.controller.machinfo.iter_wait_for_refill(check_time):
            if status == "WAIT_INJECTION":
                print("Waiting injection ...",end="\r")
            else:
                log_warning(self, "waiting injection done.")
                print("Waiting injection ... done.",end="\r")
        

    
