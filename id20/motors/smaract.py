from bliss.controllers.motors.smaract import SmarAct
from bliss.comm.util import get_comm
from bliss import global_map


class ID20SmarAct(SmarAct):
    def __init__(self, config):
        self._delay_sensor_on = config.get("sensor_on_delay", 1.0)
        super().__init__(config)

    def initialize(self):
        self.comm = get_comm(self.config.config_dict)
        global_map.register(self, children_list=[self.comm])
        # set communication mode to synchronous at controller initialization (issue #2167)
        self["CM"] = 0
