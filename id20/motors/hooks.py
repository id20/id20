from bliss.common.hook import MotionHook
from bliss.common.logtools import log_debug

import gevent


class SmaractHook(MotionHook):
    """
    Hook to control encoder signal emission

    due to smaract encoder signal emission (light), we have very high noise level on diamond beam position monitors diodes... the encoder signal is measured by the diode!
    this hook allows to switch on piezo encoder sensor only when moving, so that it is always switched off while measuring the diodes.
    """

    def __init__(self, name, config):
        self.name = name
        self.config = config
        super().__init__()

    def pre_move(self, motion_list):

        for m in motion_list:
            m.axis.controller.set_on(m.axis)
            gevent.sleep(m.axis.controller._delay_sensor_on)
            log_debug(
                self,
                "smaract sensor for {0} switched on.".format(m.axis.name),
            )

    def post_move(self, motion_list):

        for m in motion_list:

            m.axis.controller.sensor_enabled = 2
            log_debug(
                self,
                "smaract sensor for {0} switched off.".format(m.axis.name),
            )
