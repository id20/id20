============
ID20 project
============

[![build status](https://gitlab.esrf.fr/ID20/id20/badges/master/build.svg)](http://ID20.gitlab-pages.esrf.fr/ID20)
[![coverage report](https://gitlab.esrf.fr/ID20/id20/badges/master/coverage.svg)](http://ID20.gitlab-pages.esrf.fr/id20/htmlcov)

ID20 software & configuration

Latest documentation from master can be found [here](http://ID20.gitlab-pages.esrf.fr/id20)
